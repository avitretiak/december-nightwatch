module.exports = {
    
    // Careers pageObject mapping //

    url: 'http://decemberlabs.com/careers',
    elements: {
        navBar:  'nav.navigation_bar',
        mobileNav: 'a.btn_mobile',
    },
    sections: {
        body: {
            selector: 'body',
            elements: {
                applyFormSection: 'section.modal.msg_join_us',
                contentSections: 'section.row:not(.bottom_footer)',
                cultureSection: 'section.content_culture',
                applyButtons: {
                    selector :'//a[contains(text(), "Apply")]',
                    locateStrategy: 'xpath'
                }
            },
            sections: {
                applyForm: {
                    selector: 'section.modal.msg_join_us',
                    elements : {
                        nameInput: 'input[type="name"]',
                        emailInput: 'input[type="email"]',
                        phoneInput: 'input[type="tel"]',
                        linkedinInput: 'input[name*="linkedin"]',
                        cvFileDiv: 'div.row.file_input',
                        cvFileInput: 'input[name="cv_file"]',
                        commentsBox: 'textarea[name="comments"]',
                        newsletterCheck: 'div#uniform-newsletter',
                        sendButton: 'input[type="submit"]',
                        statusIllustration: 'span.illustration',
                        statusText: 'span.big_text',
                        failDiv: 'div.join-us-fail',
                    }
                }
            }
        }
    }
}