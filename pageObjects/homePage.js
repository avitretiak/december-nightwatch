module.exports = {
    
    // Home pageObject mapping //

    url: 'http://decemberlabs.com/',
    elements: {
        navBar:  'nav.navigation_bar',
        mobileNav: 'a.btn_mobile',
        careers: 'a[href*="/careers"]',
    },
}