# December Labs Nightwatch automation
Automatic test for filling and submitting the "Join Our Team" form for December Labs

Using the Nightwatch framework, further step information is included as comments in the code in `/tests/december_join.js`  
Page Object mappings can be found in the `/pageObjects` directory  
Includes `/tests/dummy.pdf`, a dummy pdf file to automate testing the CV upload in the form.  

Test should be viewport size agnostic, succeeding in both mobile and desktop sized windows.


## Requirements
- npm v6.14.8
- node v12.18.2
- Google Chrome v87.0

## Usage

1. Clone the repository

`git clone https://gitlab.com/avitretiak/december-nightwatch.git`

2. Enter the directory

`cd december-nightwatch`

3. Install dependencies via npm

`npm install`

4. Execute tests

`npx nightwatch`

5. Observe automated testing in chrome and output in console :)

## Findings

None for now :)
