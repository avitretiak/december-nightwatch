module.exports = {
    '1. Navigate to Careers page': function (browser) {
        // Set pageObject variable //
        let homePage = browser.page.homePage();

        // Navigate to homepage, check if navigation loaded, if navigation is mobile then open it, find careers link and click it //
        homePage.navigate()
            .assert.visible('@navBar', 'NavBar is visible')
            .isVisible('@mobileNav', result => {
                if (result.value) {homePage.click('@mobileNav')}
            })
            .assert.visible('@careers', 'Found careers link')
            .click('@careers');


        // Confirm navigation to careers site //
        browser.assert.urlEquals('https://decemberlabs.com/careers', 'Properly navigated to careers page');

    },

    '2. Assert careers page, and open the Join Us form, Assert it is open': function (browser) {

        // Set pageObject variable //
        let careersPage = browser.page.careersPage();


        // Check if careers page navigation loaded, store reference to the page's body pageObject in variable //
        careersPage.assert.visible('@navBar', 'Careers navbar is displayed, site loaded');
        let careersBody = careersPage.section.body;

        // Expect body to have at least one content row section //
        careersBody.expect.elements('@contentSections').count.to.not.equal(0);

        // Assert at least one content section is visible, Assert at least one "Apply now!" button is visible and contains proper text, then click it //
        careersBody
            .assert.visible('@contentSections', 'At least one content section visible')
            .assert.visible('@applyButtons', 'At least one application button visible')
            .assert.containsText('@applyButtons', 'Apply', 'Button contains text "Apply"')
            .click('@applyButtons');

        // Assert application form has the "show" class, Assert it is visible //
        careersBody
            .assert.attributeContains('@applyFormSection', 'class', 'show', 'Application form container has the "Show" class')
            .assert.visible('@applyFormSection', 'Application form is visible');
    },

    '3. Fill and submit form and Assert successful submission': function (browser) {
        
        // Set pageObject variable //
        let applyForm = browser.page.careersPage().section.body.section.applyForm;
        
        // Fill in form values and submit the form, Assert form submitted properly from success message //
        applyForm
            .setValue('@nameInput', 'Avi Tretiak')
            .setValue('@emailInput', 'test@decemberlabs.com')
            .setValue('@phoneInput', Math.floor((Math.random() * 10000000) + 1000000))
            .setValue('@linkedinInput', 'www.linkedin.com/in/avi-tretiak')
            .setValue('@cvFileInput', __dirname + '/dummyFile.pdf')
            .setValue('@commentsBox', 'This is a test comment, have a great day! :P')
            .click('@newsletterCheck')
            .click('@sendButton')
            .waitForElementVisible('@statusIllustration', 'File and form upload complete')
            .assert.not.visible('@failDiv', 'Failure message is not displayed')
            .assert.visible('@statusText', 'Form status text visible').assert.containsText('@statusText', 'Success', 'Form completed and submitted properly! :D');            
    }
}